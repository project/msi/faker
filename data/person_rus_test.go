package data

import (
	"testing"

	"gitflic.ru/project/msi/goutils/transliteration"
)

func transliterationSliceTest(s *[]string, t *testing.T) {
	var isError bool
	for _, name := range *s {
		_, err := transliteration.RuEn889(name)
		if err != nil {
			isError = true
			t.Logf("%s transliteration failed: %s\n", name, err.Error())
		}
	}
	if isError {
		t.Error("transliteration failed")
	}
}

func TestFirstNamesManRus(t *testing.T) {
	transliterationSliceTest(&FirstNamesManRus, t)
}

func TestFirstNamesWomanRus(t *testing.T) {
	transliterationSliceTest(&FirstNamesWomanRus, t)
}

func TestMidNamesManRus(t *testing.T) {
	transliterationSliceTest(&MidNamesManRus, t)
}

func TestMidNamesWomanRus(t *testing.T) {
	transliterationSliceTest(&MidNamesWomanRus, t)
}

func TestLastNamesManRus(t *testing.T) {
	transliterationSliceTest(&LastNamesManRus, t)
}

func TestLastNamesWomanRus(t *testing.T) {
	transliterationSliceTest(&LastNamesWomanRus, t)
}
