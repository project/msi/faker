package phone

import (
	"reflect"
	"regexp"
	"testing"
)

func TestRandomMobilePhoneRusLen(t *testing.T) {
	randdphone := MobilePhoneRus()

	if len(randdphone) != 11 {
		t.Error("TestRandomPhoneRus error: wrong length")
	}
}

func TestRandomMobilePhoneRusType(t *testing.T) {
	randdphone := MobilePhoneRus()
	if reflect.TypeOf(randdphone) != reflect.TypeOf("") {
		t.Error("TestRandomPhoneRus error: wrong type")
	}
}

func TestRandomMobilePhoneRusDigits(t *testing.T) {
	randdphone := MobilePhoneRus()
	matched, err := regexp.MatchString(`^\d+$`, randdphone)
	if err != nil {
		t.Error(err)
	}

	if !matched {
		t.Error("TestRandomPhoneRus error: the number must consist of numbers only")
	}
}
