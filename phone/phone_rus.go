package phone

import (
	"fmt"
	"math/rand"

	"gitflic.ru/project/msi/faker/data"
	"gitflic.ru/project/msi/goutils/struct/set"
)

var RusOperatorsCode = []int{}

func init() {
	set := set.NewSet[int]()
	set.Add(data.Mts[:]...)
	set.Add(data.Megafon[:]...)
	set.Add(data.Beeline[:]...)
	set.Add(data.Tele2[:]...)
	RusOperatorsCode = set.ElementsAsSlice()
}

// RandomPhoneRus генерирует рандомные телефонные номера
// Российских мобильных операторов mts, megafon, beeline и tele2.
func MobilePhoneRus() string {
	randOperator := RusOperatorsCode[rand.Intn(len(RusOperatorsCode))]
	randNumber := rand.Intn(9999999-1000000+1) + 1000000 // диапазон 1000000 - 9999999
	return fmt.Sprintf("7%d%d", randOperator, randNumber)
}
