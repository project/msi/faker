package main

import (
	"io"
	"os"
	"reflect"
	"strings"
	"testing"
)

func TestMail(t *testing.T) {
	// сохраняем Stdout
	saveStdout := os.Stdout

	r, w, _ := os.Pipe()
	os.Stdout = w

	// добавляем аргументы
	os.Args = []string{}
	os.Args = append(os.Args, "faker.go") // для первого аргумента args
	os.Args = append(os.Args, "emailrus")
	main()

	w.Close()
	out, _ := io.ReadAll(r)

	// восстанавливаем Stdout
	os.Stdout = saveStdout

	// получаем результат работы main
	outStr := string(out)

	if reflect.TypeOf(outStr) != reflect.TypeOf("") {
		t.Error("invalid uotput type")
	}

	if !strings.Contains(outStr, "@") {
		t.Errorf("%s invalid email address", outStr)
	}
}
