package main

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"os"
	"strings"

	"gitflic.ru/project/msi/faker"
)

func main() {
	args := prepareArgs(os.Args[1:]...)
	result, err := handler(args...)
	if err != nil {
		slog.Error(err.Error())
		fmt.Println("error, for details see log file")
		fmt.Println(help())
		return
	}
	fmt.Println(result)
}

func help() string {
	return `ABOUT
    faker - fake data generator

USAGE
    faker help - help message
    faker person - generate a new fake person
    faker person-m - generate a new fake person man
    faker person-w - generate a new fake person woman
    faker personrus - generate a new fake russian person 
    faker personrus-m - generate a new fake russian person man
    faker personrus-w - generate a new fake russian person woman
    faker emailrus - generate a new fake russain email
    faker phonerus - generate a new fake russian phone`
}

func prepareArgs(args ...string) []string {
	res := make([]string, 0, len(args))
	if len(args) < 1 {
		return []string{"help"}
	}
	for _, a := range args {
		res = append(res, strings.ToLower(a))
	}
	return res
}

func handler(args ...string) (string, error) {
	switch args[0] {

	case "help":
		return help(), nil

	case "person":
		person, err := faker.PersonAny()
		if err != nil {
			return "", err
		}
		res, err := json.Marshal(person)
		if err != nil {
			return "", err
		}
		return string(res), nil

	case "person-m":
		person, err := faker.Person("m")
		if err != nil {
			return "", err
		}
		res, err := json.Marshal(person)
		if err != nil {
			return "", err
		}
		return string(res), nil

	case "person-w":
		person, err := faker.Person("w")
		if err != nil {
			return "", err
		}
		res, err := json.Marshal(person)
		if err != nil {
			return "", err
		}
		return string(res), nil

	case "personrus":
		person, err := faker.PersonAnyRus()
		if err != nil {
			return "", err
		}
		res, err := json.Marshal(person)
		if err != nil {
			return "", err
		}
		return string(res), nil

	case "personrus-m":
		person, err := faker.PersonRus("m")
		if err != nil {
			return "", err
		}
		res, err := json.Marshal(person)
		if err != nil {
			return "", err
		}
		return string(res), nil

	case "personrus-w":
		person, err := faker.PersonRus("w")
		if err != nil {
			return "", err
		}
		res, err := json.Marshal(person)
		if err != nil {
			return "", err
		}
		return string(res), nil

	case "phonerus":
		return faker.MobilePhoneRus(), nil

	case "emailrus":
		email, err := faker.EmailRus()
		if err != nil {
			return "", err
		}
		return email, nil
	default:
		return "", fmt.Errorf("unknown argument")
	}
}
