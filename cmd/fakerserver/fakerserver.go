package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"reflect"
	"time"

	"gitflic.ru/project/msi/faker"
)

var helpMessage = `Web interface for faker
USAGE
    GET /help - help message
    GET /person - generate a new fake person
    GET /person-m - generate a new fake person man
    GET /person-w - generate a new fake person woman
    GET /personrus - generate a new fake russian person 
    GET /personrus-m - generate a new fake russian person man
    GET /personrus-w - generate a new fake russian person woman
    GET /emailrus - generate a new fake russain email
    GET /phonerus - generate a new fake russian phone
`

func main() {
	port := flag.String("port", "8080", "server port")
	flag.Parse()

	router := http.NewServeMux()
	router.HandleFunc("GET /help", helpResponse)
	router.HandleFunc("GET /{param}", mainHandler)

	server := http.Server{
		Addr:           ":8080",
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	fmt.Printf("Starting server at port :%s\n", *port)
	server.ListenAndServe()
}

func mainHandler(w http.ResponseWriter, r *http.Request) {
	param := r.PathValue("param")

	switch param {

	case "person":
		person, err := faker.PersonAny()
		if err != nil {
			errorResponse(w, err)
		}
		okResponse(w, person)

	case "person-m":
		person, err := faker.Person("m")
		if err != nil {
			errorResponse(w, err)
		}
		okResponse(w, person)

	case "person-w":
		person, err := faker.Person("w")
		if err != nil {
			errorResponse(w, err)
		}
		okResponse(w, person)

	case "personrus":
		person, err := faker.PersonAnyRus()
		if err != nil {
			errorResponse(w, err)
		}
		okResponse(w, person)

	case "personrus-m":
		person, err := faker.PersonRus("m")
		if err != nil {
			errorResponse(w, err)
		}
		okResponse(w, person)

	case "personrus-w":
		person, err := faker.PersonRus("w")
		if err != nil {
			errorResponse(w, err)
		}
		okResponse(w, person)

	case "phonerus":
		phone := faker.MobilePhoneRus()
		w.Write([]byte(phone))

	case "emailrus":
		email, err := faker.EmailRus()
		if err != nil {
			errorResponse(w, err)
		}
		okResponse(w, email)

	default:
		badRequestResponse(w)
	}
}

func okResponse(w http.ResponseWriter, data any) {
	val := reflect.ValueOf(data)

	switch val.Kind() {

	case reflect.String:
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.Write([]byte(data.(string)))

	case reflect.Int, reflect.Int8, reflect.Int32, reflect.Int64:
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.Write([]byte(fmt.Sprintf("%v", data)))

	case reflect.Map:
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.Write(responseEncoder(data))

	case reflect.Slice:
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.Write(responseEncoder(data))

	default:
		// тут обрабатываются структуры, потому application/json
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.Write(responseEncoder(data))
	}
}

func responseEncoder(data any) []byte {
	buf := &bytes.Buffer{}
	resp := json.NewEncoder(buf)
	resp.SetEscapeHTML(true)
	resp.Encode(data)
	return buf.Bytes()
}

func errorResponse(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Write([]byte(err.Error()))
}

func badRequestResponse(w http.ResponseWriter) {
	w.WriteHeader(http.StatusBadRequest)
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Write([]byte("unknown argument, use \"help\" for more info\n"))
}

func helpResponse(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Write([]byte(helpMessage))
}
