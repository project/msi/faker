package person

import (
	"math/rand"

	"gitflic.ru/project/msi/faker/data"
)

// FirstNameManRus генерирует рандомные мужские имена кириллицей.
func FirstNameManRus() string {
	return data.FirstNamesManRus[rand.Intn(len(data.FirstNamesManRus))]
}

// FirstNameWomanRus генерирует рандомные женские имена кириллицей.
func FirstNameWomanRus() string {
	return data.FirstNamesWomanRus[rand.Intn(len(data.FirstNamesWomanRus))]
}

// MiddNameManRus генерирует рандомные мужские отчества кириллицей.
func MidNameManRus() string {
	return data.MidNamesManRus[rand.Intn(len(data.MidNamesManRus))]
}

// MiddNameWomanRus генерирует рандомные женские отчества кириллицей.
func MidNameWomanRus() string {
	return data.MidNamesWomanRus[rand.Intn(len(data.MidNamesWomanRus))]
}

// LastNameManRus генерирует рандомные мужские фамилии кириллицей.
func LastNameManRus() string {
	return data.LastNamesManRus[rand.Intn(len(data.LastNamesManRus))]
}

// LastNameWomanRus генерирует рандомные женские фамилии кириллицей.
func LastNameWomanRus() string {
	return data.LastNamesWomanRus[rand.Intn(len(data.LastNamesWomanRus))]
}
