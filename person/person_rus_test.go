package person

import (
	"reflect"
	"testing"
)

func TestFirstNameManRus(t *testing.T) {
	name := FirstNameManRus()
	t.Log(name)
	if reflect.TypeOf(name) != reflect.TypeOf("") {
		t.Error("TestFirstNameManRus error: wrong type")
	}
	if name == "" {
		t.Error("TestFirstNameManRus error: empty value")
	}
}

func TestFirstNameWomanRus(t *testing.T) {
	name := FirstNameWomanRus()
	t.Log(name)
	if reflect.TypeOf(name) != reflect.TypeOf("") {
		t.Error("TestFirstNameWomanRus error: wrong type")
	}
	if name == "" {
		t.Error("TestFirstNameWomanRus error: empty value")
	}
}

func TestMidNameManRus(t *testing.T) {
	name := MidNameManRus()
	t.Log(name)
	if reflect.TypeOf(name) != reflect.TypeOf("") {
		t.Error("TestMidNameManRus error: wrong type")
	}
	if name == "" {
		t.Error("TestMidNameManRus error: empty value")
	}
}

func TestMidNameWomanRus(t *testing.T) {
	name := MidNameWomanRus()
	t.Log(name)
	if reflect.TypeOf(name) != reflect.TypeOf("") {
		t.Error("TestMidNameWomanRus error: wrong type")
	}
	if name == "" {
		t.Error("TestMidNameWomanRus error: empty value")
	}
}

func TestLastNameManRus(t *testing.T) {
	name := LastNameManRus()
	t.Log(name)
	if reflect.TypeOf(name) != reflect.TypeOf("") {
		t.Error("TestLastNameManRus error: wrong type")
	}
	if name == "" {
		t.Error("TestLastNameManRus error: empty value")
	}
}

func TestLastNameWomanRus(t *testing.T) {
	name := LastNameWomanRus()
	t.Log(name)
	if reflect.TypeOf(name) != reflect.TypeOf("") {
		t.Error("TestLastNameWomanRus error: wrong type")
	}
	if name == "" {
		t.Error("TestLastNameWomanRus error: empty value")
	}
}
