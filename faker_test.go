package faker

import (
	"reflect"
	"strings"
	"testing"

	"gitflic.ru/project/msi/faker/models"
)

func checkPerson(t *testing.T, person *models.Person) {
	t.Log(person)

	if person.FirstName == "" {
		t.Error("Person first name error")
	}
	if person.MidName == "" {
		t.Error("Person mid name error")
	}
	if person.LastName == "" {
		t.Error("Person last name error")
	}
	if person.Email == "" {
		t.Error("Person email error")
	}
	if person.MobilePhone == "" {
		t.Error("Person mobile phone error")
	}
	if person.BirthDay.Day == 0 {
		t.Error("Person birthdate day error")
	}
	if person.BirthDay.Month == 0 {
		t.Error("Person birthdate moon error")
	}
	if person.BirthDay.Year == 0 {
		t.Error("Person birthdate year error")
	}
}

func TestPersonAny(t *testing.T) {
	person, err := PersonAny()
	if err != nil {
		t.Error(err)
	}
	checkPerson(t, person)
}

func TestPersonAnyRus(t *testing.T) {
	person, err := PersonAnyRus()
	if err != nil {
		t.Error(err)
	}
	checkPerson(t, person)
}

func TestPersonMan(t *testing.T) {
	person, err := Person("m")
	if err != nil {
		t.Error(err)
	}
	checkPerson(t, person)
	if person.Sex != true {
		t.Error("Person sex error")
	}
}

func TestPersonWoman(t *testing.T) {
	person, err := Person("w")
	if err != nil {
		t.Error(err)
	}
	checkPerson(t, person)
	if person.Sex != false {
		t.Error("Person sex error")
	}
}

func TestPersonManRus(t *testing.T) {
	person, err := PersonRus("m")
	if err != nil {
		t.Error(err)
	}
	checkPerson(t, person)
	if person.Sex != true {
		t.Error("Person sex error")
	}
}

func TestPersonWomanRus(t *testing.T) {
	person, err := PersonRus("w")
	if err != nil {
		t.Error(err)
	}
	checkPerson(t, person)
	if person.Sex != false {
		t.Error("Person sex error")
	}
}

func TestPersonError(t *testing.T) {
	person, err := Person("u")
	if err != nil && err.Error() != "sex value must be \"man\", \"woman\", \"m\" or \"w\"" {
		t.Error("Person sex error")
	}
	if person != nil {
		t.Error("Person sex error")
	}
}

func TestMobilePhoneRus(t *testing.T) {
	phone := MobilePhoneRus()
	if len(phone) != 11 {
		t.Error("TestRandomPhoneRus error: wrong length")
	}

	if reflect.TypeOf(phone) != reflect.TypeOf("") {
		t.Error("TestRandomPhoneRus error: wrong type")
	}
}

func TestEmailRus(t *testing.T) {
	email, err := EmailRus()
	if err != nil {
		t.Error(err)
	}
	t.Log(email)
	if !strings.Contains(email, "@") {
		t.Errorf("%s invalid email address", email)
	}
}
