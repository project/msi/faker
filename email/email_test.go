package email

import (
	"strings"
	"testing"

	"gitflic.ru/project/msi/faker/models"
)

func TestEmailRus(t *testing.T) {
	email, err := EmailRus()
	if err != nil {
		t.Error(err)
	}
	t.Log(email)
	if !strings.Contains(email, "@") {
		t.Errorf("%s invalid email address", email)
	}
}

func TestEmailForPersonRus(t *testing.T) {
	var pers = models.Person{BirthDay: models.BirthDay{Day: 1, Month: 1, Year: 1984}}
	pers.FirstName = "Иван"
	pers.LastName = "Иванов"
	email, err := EmailForPersonRus(&pers)
	if err != nil {
		t.Error(err)
	}
	t.Log(email)
	if !strings.Contains(email, "@") {
		t.Errorf("%s invalid email address", email)
	}
}

func TestEmailForPerson(t *testing.T) {
	var pers = models.Person{BirthDay: models.BirthDay{Day: 1, Month: 1, Year: 1984}}
	pers.FirstName = "Ivan"
	pers.LastName = "Ivanov"
	email, err := EmailForPerson(&pers)
	if err != nil {
		t.Error(err)
	}
	t.Log(email)
	if !strings.Contains(email, "@") {
		t.Errorf("%s invalid email address", email)
	}
}
