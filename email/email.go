package email

import (
	"fmt"
	"strings"

	"math/rand"

	"gitflic.ru/project/msi/faker/birthday"
	"gitflic.ru/project/msi/faker/data"
	"gitflic.ru/project/msi/faker/models"
	"gitflic.ru/project/msi/faker/person"
	"gitflic.ru/project/msi/goutils/transliteration"
)

// EmailRus генерирует рандомные адреса электронной почты,
// преимущественно Российских сервисов (mail, yandex, vk, rambler ...
// и некоторых популярных иностранных - gmail, outlook, yahoo ...).
func EmailRus() (string, error) {
	var pers = models.Person{BirthDay: *birthday.NewBithDay()}
	switch rand.Intn(2) {
	case 0:
		pers.FirstName = person.FirstNameWomanRus()
		pers.LastName = person.LastNameWomanRus()
	default:
		pers.FirstName = person.FirstNameManRus()
		pers.LastName = person.LastNameManRus()
	}

	emaillCompany := data.Emails[rand.Intn(len(data.Emails))]
	randUserRu := randUserFromPerson(&pers)
	randUserEn, err := transliteration.RuEn889(randUserRu)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s@%s", strings.ToLower(randUserEn), emaillCompany), nil
}

// EmailForPersonRus генерирует рандомные адреса электронной почты,
// преимущественно Российских сервисов (mail, yandex, vk, rambler ...
// и некоторых популярных иностранных - gmail, outlook, yahoo ...).
// Для генерации использует Имя, Фамилию и дату рождения переданного пользователя.
// Ожидается, что Имя и Фамилия пользователя введены на кириллице.
func EmailForPersonRus(preson *models.Person) (string, error) {
	emaillCompany := data.Emails[rand.Intn(len(data.Emails))]
	randUserRu := randUserFromPerson(preson)
	randUserEn, err := transliteration.RuEn889(randUserRu)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s@%s", strings.ToLower(randUserEn), emaillCompany), nil
}

// EmailForPerson генерирует рандомные адреса электронной почты,
// преимущественно Российских сервисов (mail, yandex, vk, rambler ...
// и некоторых популярных иностранных - gmail, outlook, yahoo ...).
// Для генерации использует Имя, Фамилию и дату рождения переданного пользователя.
// Ожидается, что Имя и Фамилия пользователя введены английскими символами.
func EmailForPerson(preson *models.Person) (string, error) {
	emaillCompany := data.Emails[rand.Intn(len(data.Emails))]
	randUser := randUserFromPerson(preson)
	return fmt.Sprintf("%s@%s", strings.ToLower(randUser), emaillCompany), nil
}

// randUserFromPerson генерирует логин почты (текст до @) на основании переданных данных пользователя.
func randUserFromPerson(person *models.Person) string {
	switch rand.Intn(9) { // + 1 чем нужно, иначе не сработает default
	case 0:
		return person.LastName

	case 1:
		return fmt.Sprintf("%s%s", person.FirstName, person.LastName)

	case 2:
		return fmt.Sprintf("%s%d", person.LastName, person.BirthDay.Year)

	case 3:
		return fmt.Sprintf("%s.%d", person.LastName, person.BirthDay.Year)

	case 4:
		return fmt.Sprintf("%s.%02d.%d", person.LastName, person.BirthDay.Month, person.BirthDay.Year)

	case 5:
		return fmt.Sprintf("%s.%02d%d", person.LastName, person.BirthDay.Month, person.BirthDay.Year)

	case 6:
		return fmt.Sprintf("%s%02d%d", person.LastName, person.BirthDay.Month, person.BirthDay.Year)

	case 7:
		return fmt.Sprintf("%s%s", person.FirstName, person.LastName)

	default:
		return fmt.Sprintf("%s.%s", person.FirstName, person.LastName)
	}
}
