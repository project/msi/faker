package faker

import (
	"fmt"
	"strings"

	"math/rand"

	"gitflic.ru/project/msi/faker/birthday"
	"gitflic.ru/project/msi/faker/email"
	"gitflic.ru/project/msi/faker/models"
	"gitflic.ru/project/msi/faker/person"
	"gitflic.ru/project/msi/faker/phone"
	"gitflic.ru/project/msi/goutils/transliteration"
)

// PersonAny создает новую фейковую персону.
func PersonAny() (*models.Person, error) {
	pers := models.Person{
		BirthDay:    *birthday.NewBithDay(),
		MobilePhone: phone.MobilePhoneRus(),
	}
	switch rand.Intn(2) {
	case 0:
		if err := man(&pers); err != nil {
			return nil, err
		}
		return &pers, nil

	default:
		if err := woman(&pers); err != nil {
			return nil, err
		}
		return &pers, nil
	}
}

// PersonAnyRus создает новую фейковую персону с ФИО кирилицей.
func PersonAnyRus() (*models.Person, error) {
	pers := models.Person{
		BirthDay:    *birthday.NewBithDay(),
		MobilePhone: phone.MobilePhoneRus(),
	}
	switch rand.Intn(2) {
	case 0:
		if err := manRus(&pers); err != nil {
			return nil, err
		}
		return &pers, nil

	default:
		if err := womanRus(&pers); err != nil {
			return nil, err
		}
		return &pers, nil
	}
}

// Person создает новую фейковую персону.
// sex - пол, может принимать значения "man", "woman", "m" или "w".
func Person(sex string) (*models.Person, error) {
	pers := models.Person{
		BirthDay:    *birthday.NewBithDay(),
		MobilePhone: phone.MobilePhoneRus(),
	}
	switch strings.ToLower(sex) {
	case "man", "m":
		if err := man(&pers); err != nil {
			return nil, err
		}
		return &pers, nil

	case "woman", "w":
		if err := woman(&pers); err != nil {
			return nil, err
		}
		return &pers, nil

	default:
		return nil, fmt.Errorf("sex value must be \"man\", \"woman\", \"m\" or \"w\"")
	}
}

// PersonRus создает новую фейковую персону с ФИО кирилицей.
// sex - пол, может принимать значения "man", "woman", "m" или "w".
func PersonRus(sex string) (*models.Person, error) {
	pers := models.Person{
		BirthDay:    *birthday.NewBithDay(),
		MobilePhone: phone.MobilePhoneRus(),
	}
	switch strings.ToLower(sex) {
	case "man", "m":
		if err := manRus(&pers); err != nil {
			return nil, err
		}
		return &pers, nil

	case "woman", "w":
		if err := womanRus(&pers); err != nil {
			return nil, err
		}
		return &pers, nil

	default:
		return nil, fmt.Errorf("sex value must be \"man\", \"woman\", \"m\" or \"w\"")
	}
}

func man(pers *models.Person) error {
	pers.Sex = true
	fName := person.FirstNameManRus()
	fNameEn, err := transliteration.RuEn889(fName)
	if err != nil {
		return err
	}
	pers.FirstName = fNameEn

	mName := person.MidNameManRus()
	mNameEn, err := transliteration.RuEn889(mName)
	if err != nil {
		return err
	}
	pers.MidName = mNameEn

	lName := person.LastNameManRus()
	lNameEn, err := transliteration.RuEn889(lName)
	if err != nil {
		return err
	}
	pers.LastName = lNameEn
	eml, err := email.EmailForPerson(pers)
	if err != nil {
		return err
	}
	pers.Email = eml
	return nil
}

func manRus(pers *models.Person) error {
	pers.Sex = true
	pers.FirstName = person.FirstNameManRus()
	pers.MidName = person.MidNameManRus()
	pers.LastName = person.LastNameManRus()

	// пользователь для почты
	fNameEn, err := transliteration.RuEn889(pers.FirstName)
	if err != nil {
		return err
	}
	lNameEn, err := transliteration.RuEn889(pers.LastName)
	if err != nil {
		return err
	}
	persForEmail := &models.Person{
		FirstName: fNameEn,
		LastName:  lNameEn,
		BirthDay:  pers.BirthDay,
	}

	eml, err := email.EmailForPerson(persForEmail)
	if err != nil {
		return err
	}
	pers.Email = eml
	return nil
}

func woman(pers *models.Person) error {
	pers.Sex = false
	fName := person.FirstNameWomanRus()
	fNameEn, err := transliteration.RuEn889(fName)
	if err != nil {
		return err
	}
	pers.FirstName = fNameEn

	mName := person.MidNameWomanRus()
	mNameEn, err := transliteration.RuEn889(mName)
	if err != nil {
		return err
	}
	pers.MidName = mNameEn

	lName := person.LastNameWomanRus()
	lNameEn, err := transliteration.RuEn889(lName)
	if err != nil {
		return err
	}
	pers.LastName = lNameEn
	eml, err := email.EmailForPerson(pers)
	if err != nil {
		return err
	}
	pers.Email = eml
	return nil
}

func womanRus(pers *models.Person) error {
	pers.Sex = false
	pers.FirstName = person.FirstNameWomanRus()
	pers.MidName = person.MidNameWomanRus()
	pers.LastName = person.LastNameWomanRus()

	// пользователь для почты
	fNameEn, err := transliteration.RuEn889(pers.FirstName)
	if err != nil {
		return err
	}
	lNameEn, err := transliteration.RuEn889(pers.LastName)
	if err != nil {
		return err
	}
	persForEmail := &models.Person{
		FirstName: fNameEn,
		LastName:  lNameEn,
		BirthDay:  pers.BirthDay,
	}

	eml, err := email.EmailForPerson(persForEmail)
	if err != nil {
		return err
	}
	pers.Email = eml
	return nil
}

// MobilePhoneRus генерирует рандомные телефонные номера
// Российских мобильных операторов mts, megafon, beeline и tele2.
func MobilePhoneRus() string {
	return phone.MobilePhoneRus()
}

// EmailRus генерирует рандомные адреса электронной почты,
// преимущественно Российских сервисов (mail, yandex, vk, rambler ...
// и некоторых популярных иностранных - gmail, outlook, yahoo ...).
func EmailRus() (string, error) {
	return email.EmailRus()
}
