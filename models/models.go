package models

type BirthDay struct {
	Day   int `json:"day"`
	Month int `json:"month"`
	Year  int `json:"year"`
}

type Person struct {
	FirstName   string   `json:"first_name"`
	MidName     string   `json:"mid_name"`
	LastName    string   `json:"last_name"`
	BirthDay    BirthDay `json:"birthday"`
	MobilePhone string   `json:"mobile_phone"`
	Email       string   `json:"email"`
	Sex         bool     `json:"sex"`
}
