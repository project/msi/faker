package birthday

import (
	"testing"
	"time"
)

func TestBithDate(t *testing.T) {
	bd := NewBithDay()
	t.Log(bd)
	if bd.Day <= 0 || bd.Day > 31 {
		t.Errorf("%d 1 has an incorrect value", bd.Day)
	}
	if bd.Month <= 0 || bd.Month > 12 {
		t.Errorf("%d 1 has an incorrect value", bd.Day)
	}
	if bd.Year <= 0 || bd.Year > time.Now().Year() {
		t.Errorf("%d 1 has an incorrect value", bd.Day)
	}
}

func TestBithDateRange(t *testing.T) {
	minAge := 40
	maxAge := 40
	bd := NewBithDay(minAge, maxAge)
	t.Log(bd)

	// текущая дата
	nowDate := time.Now()

	// https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
	loc, err := time.LoadLocation("Europe/Moscow")
	if err != nil {
		t.Error(err)
	}

	// день рождения пользователя в формате time.Time
	userDate := time.Date(bd.Year, time.Month(bd.Month), bd.Day, 0, 0, 0, 0, loc)
	// минимально возможный день рождения пользователя в формате time.Time
	minDate := time.Date(nowDate.Year()-minAge+1, time.Month(0), 0, 0, 0, 0, 0, loc)
	// максимально возможный день рождения пользователя в формате time.Time
	maxDate := time.Date(nowDate.Year()-maxAge, time.Month(0), 0, 0, 0, 0, 0, loc)

	// определяем продолжительность для каждого дня рождения
	durationUser := nowDate.Sub(userDate)
	durationMin := nowDate.Sub(minDate)
	durationMax := nowDate.Sub(maxDate)

	// выполняем проверку, что сгерерированный возраст попадает в диапазон.
	if durationUser < durationMin || durationUser > durationMax {
		t.Error("age range error")
	}
}
