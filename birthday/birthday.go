package birthday

import (
	"math/rand"
	"time"

	"gitflic.ru/project/msi/faker/models"
)

// NewBithDate генерирует случайную дату рождения
// в диапазоне от 18 до 80 лет на текущую дату.
// Диапазон можно изменить, указав свои значения
// (например NewBithDate(25, 60)).
func NewBithDay(limits ...int) *models.BirthDay {
	start := 18
	end := 80
	if len(limits) == 2 {
		start = limits[0]
		end = limits[1]
	}
	currenDate := time.Now()
	day := rand.Intn(28) + 1
	month := rand.Intn(12) + 1
	startYear := currenDate.Year() - end
	endYear := currenDate.Year() - start
	year := rand.Intn(endYear-startYear+1) + startYear

	// дополнительнпя проверка, если выпал крайний год,
	// требуемый возраст мог еще не наступить.
	if year == endYear {
		if month > int(currenDate.Month()) {
			month = rand.Intn(int(currenDate.Month())) + 1
		}
		if month == int(currenDate.Month()) {
			if day > currenDate.Day() {
				day = rand.Intn(currenDate.Day()) + 1
			}
		}
	}
	return &models.BirthDay{Day: day, Month: month, Year: year}
}
